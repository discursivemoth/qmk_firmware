#include "foobar.h"
#include "action_layer.h"
#include "eeconfig.h"

extern keymap_config_t keymap_config;

// Each layer gets a name for readability, which is then used in the keymap matrix below.
// The underscores don't mean anything - you can have a layer called STUFF or any other name.
// Layer names don't all need to be of the same length, obviously, and you can also skip them
// entirely and just use numbers.
#define _QWERTY 0
#define _COLEMAK 1
#define _LAYER1 2
#define _LAYER2 3
#define _LAYER3 4
#define _LAYER4 5
#define _LAYER5 16
enum custom_keycodes {
  QWERTY = SAFE_RANGE,
  COLEMAK,
  LAYER1,
  LAYER2,
  LAYER3,
  LAYER4,
  LAYER5,
};

// Fillers to make layering more clear
#define _______ KC_TRNS
#define XXXXXXX KC_NO

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

/* Qwerty
 * ,------------------------------------    ,---------------------------------.
 * |   Q  |   W  |   E  |   R  |   T   |    |  Y  |   U  |   I  |   O  |   P  |
 * |------+------+------+------+--------    |-----+------+------+------+------|
 * |   A  |   S  |   D  |   F  |   G   |    |  H  |   J  |   K  |   L  | ESC  |
 * |------+------+------+------+-------|    |-----+------+------+------+------|
 * |   Z  |   X  |   C  |   V  | BKSPC |    |SPACE|  B   |   N  |   M  | ENTER|
 * |------+------+------+------+-------+    +-----+------+------+------+------|
 */
[_QWERTY] = KEYMAP( \
  KC_Q,    KC_W,    KC_E,    KC_R,    KC_T,    KC_Y,    KC_U,    KC_I,    KC_O, KC_P, \
  KC_A,    KC_S,    KC_D,    KC_F,    KC_G,    KC_H,    KC_J,    KC_K,    KC_L, KC_ESC, \
  LT(COLEMAK, KC_Z), LT(LAYER1, KC_X), LT(LAYER2, KC_C), LT(LAYER3, KC_V),KC_BSPC, KC_SPC, LT(LAYER4, KC_B), LT(LAYER5, KC_N), KC_M, KC_ENT \
),

/* Colemak
 * ,------------------------------------    ,---------------------------------.
 * |   Q  |   W  |   F  |   P  |   G   |    |  J  |   L  |   U  |   Y  | ESC  |
 * |------+------+------+------+--------    |-----+------+------+------+------|
 * |   A  |   R  |   S  |   T  |   D   |    |  H  |   N  |   E  |   I  |   O  |
 * |------+------+------+------+-------|    |-----+------+------+------+------|
 * |   Z  |   X  |   C  |   V  | BKSPC |    |SPACE|  B   |   K  |   M  |ENTER |
 * |------+------+------+------+-------+    +-----+------+------+------+------|
 */
[_COLEMAK] = KEYMAP( \
  KC_Q,    KC_W,    KC_F,    KC_P,    KC_G,    KC_J,    KC_L,    KC_U,    KC_Y, KC_ESC, \
  KC_A,    KC_R,    KC_S,    KC_T,    KC_D,    KC_H,    KC_N,    KC_E,    KC_I, KC_O, \
  LT(COLEMAK, KC_Z), LT(LAYER1, KC_X), LT(LAYER2, KC_C), LT(LAYER3, KC_V),KC_BSPC, KC_SPC, LT(LAYER4, KC_B), LT(LAYER5, KC_K), KC_M, KC_ENT \
),

/* LAYER1
 * ,------------------------------------    ,---------------------------------.
 * |   1  |   2  |   3  |   4  |   5   |    |  6  |   7  |   8  |   9  |   0  |
 * |------+------+------+------+--------    |-----+------+------+------+------|
 * |   F1 |  F2  |  F3  |  F4  |  F5   |    |  F6 |  F7  |  F8  |  F9  | F10  |
 * |------+------+------+------+-------|    |-----+------+------+------+------|
 * |TRANS | TRANS| TRANS|TRANS | DEL   |    |TRANS|TRANS |TRANS |TRANS |TRANS |
 * |------+------+------+------+-------+    +-----+------+------+------+------|
 */
[_LAYER1] = KEYMAP( \
  KC_1,    KC_2,    KC_3,    KC_4,    KC_5,    KC_6,    KC_7,    KC_8,    KC_9,    KC_0, \
  KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5,   KC_F6,   KC_F7,   KC_F8,   KC_F9,   KC_F10, \
  _______, _______, _______, _______, KC_DEL,  _______, _______, _______, _______, _______ \
),

/* LAYER2
 * ,------------------------------------    ,---------------------------------.
 * |   !  |   @  |   #  |   $  |   %   |    |  ^  |   &  |   *  |   (  |   )  |
 * |------+------+------+------+--------    |-----+------+------+------+------|
 * |  F11 | F12  | TRANS| TRANS| TRANS |    |TRANS| TRANS| TRANS| TRANS| TRANS|
 * |------+------+------+------+-------|    |-----+------+------+------+------|
 * |TRANS | TRANS| TRANS|TRANS | TRANS |    |TRANS|TRANS |TRANS |TRANS |TRANS |
 * |------+------+------+------+-------+    +-----+------+------+------+------|
 */
[_LAYER2] = KEYMAP( \
  KC_EXLM,  KC_AT,  KC_HASH, KC_DLR,  KC_PERC, KC_CIRC, KC_AMPR, KC_ASTR, KC_LPRN, KC_RPRN, \
  KC_F11,   KC_F12, _______, _______, _______, _______, _______, _______, _______, _______, \
  _______, _______, _______, _______, _______, _______, _______, _______, _______, _______ \
),

/* LAYER3
 * |------+------+------+------+-------|    |-----+------+------+------+------|
 * |TRANS | TRANS| TRANS|TRANS | TRANS |    |  -  |   =  |   [  |   ]  |   \  |
 * |------+------+------+------+-------+    +-----+------+------+------+------|
 * | TAB  | TRANS| TRANS|TRANS | TRANS |    |  ,  |   .  |   /  |   ;  |   '  |
 * |------+------+------+------+-------+    +-----+------+------+------+------|
 * |TRANS | TRANS| TRANS|TRANS | TRANS |    |TRANS| LEFT | DOWN |  UP  | RIGHT|
 * |------+------+------+------+-------+    +-----+------+------+------+------|
 */
[_LAYER3] = KEYMAP( \
  _______, _______, _______, _______, _______, KC_PMNS, KC_PEQL, KC_LBRC, KC_RBRC, KC_BSLS, \
  KC_TAB,  _______, _______, _______, _______, KC_PCMM, KC_DOT,  KC_SLSH, KC_SCLN, KC_QUOT, \
  _______, _______, _______, _______, _______, _______, KC_LEFT, KC_DOWN, KC_UP, KC_RIGHT \
),

/* LAYER4
 * |------+------+------+------+-------|    |-----+------+------+------+------|
 * |TRANS | TRANS| TRANS|TRANS | TRANS |    |  _  |   +  |   {  |   }  |   |  |
 * |------+------+------+------+-------+    +-----+------+------+------+------|
 * | TAB  | TRANS| TRANS|TRANS | TRANS |    |  <  |   >  |   ?  |   :  |   "  |
 * |------+------+------+------+-------+    +-----+------+------+------+------|
 * |TRANS | TRANS| TRANS|TRANS | TRANS |    |TRANS| HOME | PGDWN| PGUP |  END |
 * |------+------+------+------+-------+    +-----+------+------+------+------|
 */
[_LAYER4] = KEYMAP( \
  _______, _______, _______, _______, _______, KC_UNDS, KC_PLUS, KC_LCBR, KC_RCBR, KC_PIPE, \
  KC_TAB,  _______, _______, _______, _______, KC_LT, KC_GT,  KC_QUES, KC_COLN, KC_DQT, \
  _______, _______, _______, _______, _______, _______, KC_HOME, KC_PGUP, KC_PGDN, KC_END \
),
/* LAYER5
 * |------+------+------+------+------|    |------+------+------+------+-------|
 * |QWERTY|COLEMK|LAYER1|LAYER2|LAYER3|    |LAYER4|LAYER5| TRANS|TRANS |PRNTSCR|
 * |------+------+------+------+------+    |------+------+------+------+-------+
 * |TRANS | TRANS| TRANS|TRANS | TRANS|    |TRANS | TRANS| TRANS|  +BL |  -BL  |
 * |------+------+------+------+------+    |------+------+------+------+-------+
 * |TRANS | TRANS| TRANS|TRANS | TRANS|    |TRANS | TRANS| TRANS|TRANS | TRANS |
 * |------+------+------+------+------+    |------+------+------+------+-------+
 */
[_LAYER5] = KEYMAP( \
  TO(QWERTY), TO(COLEMAK), TO(LAYER1), TO(LAYER2), TO(LAYER3), TO(LAYER4), TO(LAYER5), _______, _______, KC_PSCR, \
  _______,  _______, _______, _______, _______,                _______, _______,  _______, BL_INC, BL_DEC, \
  _______,  _______, _______, _______, _______,                _______, _______, _______, _______, _______ \
)

};


void persistent_default_layer_set(uint16_t default_layer) {
  eeconfig_update_default_layer(default_layer);
  default_layer_set(default_layer);
}

